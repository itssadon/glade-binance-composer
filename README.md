# glade/binance
💳 📦 💰
This is a Composer library that simplifies creating an account on binace and gives access to buying and selling of crypto currency.

## About Glade
Glade is powering growth for innovative African enterprises. We are fundamentally changing how small businesses access banking and financial services. A modern digital bank that enables small businesses in Africa to thrive.

To Learn more, visit [https://glade.ng/](https://glade.ng/).

## Getting Started
You need a Merchant ID, API Key and Secret to authenticate against the binance API, please contact support@gladepay.com to setup a demo account.

## Installation
You can close this project and include the class in your project or you can add the repo in your `composer.json` file as in the example below:

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/itssadon/glade-binance-composer"
        }
    ],
    "require": {
        "glade/binance": "master",
    }
}
```

Or simply run the command `composer require glade/binance` in your project.

## Usage
Initiate Class:
```
use Glade\Binance\Member;

$gladeBinanceMember = new Member($merchant_code, $api_key, $api_secret, $base_url);
```

### Making calls
```
$response = $gladeBinanceMember->createMember('president@email.com');
return $response;
```

### Resources 📔
    - Member (Member account managemnt)
        - createMember (Create a binance user for glade merchants.)
        - bindMember (Let user bind their binance account to glade.)
        - getBindStatus (Check the binding status for the given accountId.)
    
    - Trade (Trade API)
        - getQuote (Get trade quotation information.)
        - buyCrypto (Used to buy crypto currency.)
        - getTransactions (Get transaction information by your order id or your user account id.)
        - settleTransaction (Transaction settlement by orderId and paymentId when you confirm the order has been finished successfully.)

## Tests

## Contributing 🎸 💻
In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality and test your code.

Kindly [follow me on twitter](https://twitter.com/itssadon)!

## License
The MIT License (MIT). Please see [License File](LICENSE) for more information.
