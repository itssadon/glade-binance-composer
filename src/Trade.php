<?php
namespace Glade\Binance;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
*  Trade API
*
*  This class manages binance user for our customers.
*  
*
*  @author Abubakar Sadiq Hassan
*/
class Trade{

    /**  @var string $client for instannce of http client responsible for making requests to the binance api server */
    private $client;
    private $merchant_code;
    private $api_key;
    private $api_secret;
    private $base_url;
    private $headers;

    /**
     * Constructor
     *
     * Used to instanciate the class.
     *
     * @param string $merchant_code A string containing the client id allocated by Binance.
     * @param string $x_api_key A string containing the api key allocated by Binance.
     * @param string $x_api_signature A string containing the signature (A signature is actually a string hashed by SHA256 and there are two rules to construct a signature).
     * @param string $base_url A string containng the binace api url.
     *
     * @return string
     */
    public function __construct($merchant_code, $api_key, $api_secret, $base_url){
        $this->merchant_code = $merchant_code;
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;
        $this->base_url = $base_url;

        $this->client = new Client(['base_uri' => $base_url]);
    }

    /**
     * Call API
     * 
     * Used to make calls to binance api.
     * 
     * @param $method
     * @param $api_resource
     * @param $data
     *
     * @return (array | object)
     */
    private function callAPI($method, $api_resource, $data){
        $x_api_signature;
        $merchant_code = $this->merchant_code;
        $secret = $this->api_secret;
        $timestamp = strtotime("now");
        $api_key = $this->api_key;

        $headers = [
            'Content-Type' => 'application/json',
            'merchantCode' => $merchant_code,
            'x-api-key' => $api_key,
            'timestamp' => $timestamp
        ];
        
        if (empty($data)){
            $x_api_signature = "merchantCode=$merchant_code&timestamp=$timestamp&x-api-key=$api_key&secret=$secret";
            $headers['x-api-signature'] = $x_api_signature;
            
            $response = $this->client->request($method, $api_resource, ['headers' => $headers]);
        } else {
            $strData = implode(',', array_map(array($this, 'out'), array_keys($data), $data));
            $x_api_signature = "$strData&merchantCode=$merchant_code&timestamp=$timestamp&x-api-key=$api_key&secret=$secret";
            $headers["x-api-signature"] = $x_api_signature;

            $response = $this->client->request($method, $api_resource, ['headers' => $headers, 'body' => json_encode($data)]);
        }

        $response_body = json_decode($response->getBody());
        return $response_body;
    }

    private function out($a, $b){
        return $a .'=' . $b;
    }

    /**
     * Request Quote
     *
     * Used to get trade quotation information.
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     *
     * @return string
     */
    public function getQuote($crypto_currency, $base_currency, $requested_currency, $requested_amount, $pay_type, $binance_user_id, $merchant_user_account){
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/trade/getQuote';
        $request_data = [
            "cryptoCurrency" => $crypto_currency,
            "baseCurrency" => $base_currency,
            "requestedCurrency" => $requested_currency,
            "requestedAmount" => $requested_amount,
            "payType" => $pay_type,
            "binanceUserId" => $binance_user_id,
            "merchantUserAccount" => $merchant_user_account
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }

    /**
     * Buy Crypto
     *
     * Used to by crypto currency.
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     *
     * @return string
     */
    public function buyCrypto($binance_user_id, $merchant_user_account, $quote_id, $order_id, $note){
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/trade/execute';
        $request_data = [
            "binanceUserId" => $binance_user_id,
            "merchantUserAccount" => $merchant_user_account,
            "quoteId" => $quote_id,
            "orderId" => $order_id,
            "note" => $note
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }

    /**
     * Order query
     *
     * Get transaction information by your order id or your user account id.
     * This endpoint will fetch the latest 100 transactions if has
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     *
     * @return string
     */
    public function getTransactions($merchant_user_account, $order_id){
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/trade/getTransactions';
        $request_data = [
            "merchantUserAccount" => $merchant_user_account,
            "orderId" => $order_id
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }

    /**
     * Transaction Settle
     *
     * Transaction settle by orderId and paymentId when you confirm the order has been finished successfully.
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     *
     * @return string
     */
    public function settleTransaction($order_id, $payment_id){
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/trade/transactionSettle';
        $request_data = [
            "orderId" => $order_id,
            "paymentId" => $payment_id
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }
}