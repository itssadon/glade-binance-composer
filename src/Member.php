<?php
namespace Glade\Binance;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
*  Member management
*
*  This class manages binance user for our customers.
*  
*
*  @author Abubakar Sadiq Hassan
*/
class Member{

    /**  @var string $client for instannce of http client responsible for making requests to the binance api server */
    private $client;
    private $merchant_code;
    private $api_key;
    private $api_secret;
    private $base_url;
    private $headers;

    /**
     * Constructor
     *
     * Used to instanciate the class.
     *
     * @param string $merchant_code A string containing the client id allocated by Binance.
     * @param string $x_api_key A string containing the api key allocated by Binance.
     * @param string $x_api_signature A string containing the signature (A signature is actually a string hashed by SHA256 and there are two rules to construct a signature).
     * @param string $base_url A string containng the binace api url.
     *
     * @return string
     */
    public function __construct($merchant_code, $api_key, $api_secret, $base_url){
        $this->merchant_code = $merchant_code;
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;
        $this->base_url = $base_url;

        $this->client = new Client(['base_uri' => $base_url]);
    }

    /**
     * Call API
     * 
     * Used to make calls to binance api.
     * 
     * @param $method
     * @param $api_resource
     * @param $data
     *
     * @return (array | object)
     */
    private function callAPI($method, $api_resource, $data){
        $x_api_signature;
        $merchant_code = $this->merchant_code;
        $secret = $this->api_secret;
        $timestamp = strtotime("now");
        $api_key = $this->api_key;

        $headers = [
            'Content-Type' => 'application/json',
            'merchantCode' => $merchant_code,
            'x-api-key' => $api_key,
            'timestamp' => $timestamp
        ];
        
        if (empty($data)){
            $x_api_signature = "merchantCode=$merchant_code&timestamp=$timestamp&x-api-key=$api_key&secret=$secret";
            $headers['x-api-signature'] = $x_api_signature;

            $response = $this->client->request($method, $api_resource, ['headers' => $headers]);
        } else {
            $strData = implode(',', array_map(array($this, 'out'), array_keys($data), $data));
            $x_api_signature = "$strData&merchantCode=$merchant_code&timestamp=$timestamp&x-api-key=$api_key&secret=$secret";
            $headers["x-api-signature"] = $x_api_signature;
            
            $response = $this->client->request($method, $api_resource, ['headers' => $headers, 'body' => json_encode($data)]);
        }

        $response_body = json_decode($response->getBody());
        return $response_body;
    }

    private function out($a, $b){
        return $a .'=' . $b;
    }
    
    /**
     * Create member
     *
     * Used to create a binance user for glade merchants.
     * this helps the phpdocumentator to properly generator the documentation
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     *
     * @return string
     */
    public function createMember($merchant_user_account){
        $ip_address;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){ //whether ip is from share internet
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ //whether ip is from proxy
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {//whether ip is from remote address
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/user/register';
        $request_data = [
            "merchantUserAccount" => $merchant_user_account,
            "userIp" => $ip_address
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }

    /**
     * Bind existing user account
     *
     * Used to let user bind their binance account to your platform.
     * Redirect to Binance login page. User enter the email and password to login with binance account.
     * After this action user can finish the bind member.
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     * 
     * @return string
     */
    public function bindMember($merchant_user_account, $redirect_url){
        $merchant_code = $this->merchant_code;
        $binance_redirect_url = "https://www.binance.com/***.html?merchantCode=$merchant_code&merchantUserAccount=$merchant_user_account&redirect=$redirect_url";
        $response = [
            "code" => 200,
            "message" => 'Redirect to Binance login page. User should enter the email and password to login with binance account. After this action user can finish the bind member.',
            "redirectUrl" => $binance_redirect_url
        ];

        return json_encode($response);
    }

    /**
     * Get binding status
     *
     * Check the binding status for the given accountId.
     *
     * @param string $merchant_user_account A string containing the user‘s email which is register in glade platform.
     * 
     * @return string
     */
    public function getBindStatus($merchant_user_account){
        $request_method = 'POST';
        $request_resource = '/gateway-api/v1/public/ocbs/user/bindingStatus';
        $request_data = [
            "merchantUserAccount" => $merchant_user_account
        ];

        return $this->callAPI($request_method, $request_resource, $request_data);
    }
}